(function () {
    'use strict';

    function blockForgotPasswordForm (PATH_CONFIG, $rootScope, $translate, $location, API_Service) {
        return {
            restrict: "E",
            replace: "true",
            link: function (scope, element, attrs) {

                element.on('active focus', '.form-control', function (e) {
                    e.stopPropagation();
                    $(this).parent().addClass('focused', (this.value.length > 0));
                });

                element.on('blur change', '.form-control', function (e) {
                    e.stopPropagation();

                    $(this).parent().toggleClass('focused', (this.value.length > 0));

                    var focused = $('.focused');

                    focused.each(function (index, obj) {
                        if ($(this).find('select').val() == '?') {
                            $(this).removeClass('focused');
                        }
                    });
                }).trigger('blur');

            },


            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                $scope.form_name = 'forgot_password_form';

                $scope.model = {
                    "client_id": data.dev_client_id,
                    "client_secret": data.dev_client_secret,
                    "grant_type": data.grant_type
                };

                $scope.schema = {
                    "type": "object",
                    "title": "Comment",
                    "properties": {
                        "client_id": {
                            "title": "client_id",
                            "type": "string"
                        },
                        "client_secret": {
                            "title": "client_secret",
                            "type": "string"
                        },
                        "grant_type": {
                            "title": "grant_type",
                            "type": "string"
                        },
                        "email":  {
                            "title": $translate.instant('form.email'),
                            "type": "string",
                            "pattern": "^\\S+@\\S+$",
                            "validationMessage": $translate.instant('form.error.email')
                        }
                    },
                    "required": ["email"]
                };

                $scope.form = [
                    {
                        "key": "client_id",
                        "type": "hidden"
                    },
                    {
                        "key": "client_secret",
                        "type": "hidden"
                    },
                    {
                        "key": "grant_type",
                        "type": "hidden"
                    },
                    {
                        "key": "email",
                        "required": true
                    }
                ];

                if (typeof $scope.module != 'undefined') {

                    $scope.showError = false;
                    $scope.thankyou_show = false;

                }

                $scope.post_url = host + 'oauth/token';

                $scope.link = $location.path();

                $scope.triggerSubmit = function() {
                    $('.block-newsletter-form__form').trigger('submit');
                };

                $scope.onSubmit = function (form) {

                    form.preventDefault();

                    // First we broadcast an event so all fields validate themselves
                    $scope.$broadcast('schemaFormValidate');

                    // Then we check if the form is valid
                    if ($scope[$scope.form_name].$valid) {

                        var formData = {};

                        angular.forEach($scope.schema.properties, function (value, keys) {
                            formData[keys] = $scope[$scope.form_name][keys].$viewValue;
                        });

                        API_Service($scope.post_url, {}).save_no_array({}, formData,
                            function success(data) {

                                $scope.showError = false;
                                $scope.form_success = true;
                                $scope.thankyou_show = true;

                                $scope.model = {
                                    "client_id": data.dev_client_id,
                                    "client_secret": data.dev_client_secret,
                                    "grant_type": data.grant_type
                                };

                                $rootScope.$emit ('loggedIn', data);

                            },

                            function error(data) {
                                $scope.showError = true;

                                $scope.error = data.data.errors[0];
                            });
                    }

                };

            },
            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-forgot-password-form/block-forgot-password-form.html'
        }
    }

    blockForgotPasswordForm.$inject = ['PATH_CONFIG', '$rootScope', '$translate', '$location', 'API_Service'];

    angular
        .module('bravoureAngularApp')
        .directive('blockForgotPasswordForm', blockForgotPasswordForm);
})();
